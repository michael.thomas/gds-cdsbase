%define srcname gds

Name: 		gds-cdsbase
Summary: 	DTT v3
Version: 	2.18.17+cds5
Release: 	5%{?dist}
License: 	GPL
Group: 		CDS
Source: 	%{srcname}-%{version}.tar.gz
Packager: 	Erik von Reis (erik.vonreis@ligo.org)
BuildRoot: 	%{_tmppath}/gds-%{version}-root
URL: 		http://git.ligo.org/erik.vonreis/gds
BuildRequires: 	gcc, gcc-c++, glibc, automake, autoconf, libtool, m4, make
BuildRequires:  gzip zlib bzip2 expat-devel libXpm-devel ldas-tools-framecpp
BuildRequires:  ldas-tools-framecpp-devel ldas-tools-al-devel libmetaio-devel
BuildRequires:  root, libcurl-devel zlib-devel hdf5-devel krb5-devel numpy
BuildRequires:  python-devel readline-devel fftw-devel cyrus-sasl-devel swig
BuildRequires:  jsoncpp-devel
Summary: 	GDS libraries needed to support cds-crtools
Provides: 	%srcname
Obsoletes:	%srcname < %version, gds-core, gds-lowlatency
Obsoletes:	gds-headers, gds-lowlatency-headers, gds-core, gds-lowlatency, gds-crtools

%description
These libraries are repacked from GDS to support the CDS distribution of control room tools.

%package devel
Summary:	Headers and libraries for building against %{name}
%description devel
Headers and libraries for building against %{name}

%prep
%setup -q -n %{srcname}-%{version}

%build
PKG_CONFIG_PATH=%{_libdir}/pkgconfig
ROOTSYS=%{_prefix}

export PKG_CONFIG_PATH ROOTSYS
%configure  --includedir=%{_prefix}/include/%{srcname} \
	     --enable-only-dtt 
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
rm -f $RPM_BUILD_ROOT%{_libdir}/*.la

%files
%defattr(-,root,root)
%{_bindir}/*
%{_libdir}/*.so.*
%{_libdir}/*.pcm
%{_prefix}/etc/gds-user-env.sh
%{_datarootdir}/gds/setup/root-setup

%files devel
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
%{_libdir}/*.a

%changelog
* Mon Jun 29 2020 Erik von Reis <evonreis@caltech.edu> - 2.18.17+cds5-5
- Bump revision to force rebuild

* Thu Jun 18 2020 Erik von Reis <evonreis@caltech.edu> - 2.18.17+cds5-4
- Obsoletes gds-crtools

* Sat Jun 13 2020 Michael Thomas <michael.thomas@LIGO.ORG> - 2.18.17+cds5-3
- Split headers, libraries into -devel subpackage.  Spec file cleanup

* Thu Jun 11 2020 Erik von Reis <evonreis@caltech.edu> - 2.18.17+cds5-2
- some build cleanup

* Thu Jan 30 2020 Michael Thomas <michael.thomas@LIGO.ORG> 2.18.17-1
- Initial package based on refactored source code
